$(document).ready(function () {
    draw();
});

function draw() {
    $("#list_badges").simpletable({
        getURL: function () {
            return "/user";
        },
        dataFormatter: function (dataraw) {
            return dataraw._embedded.user;
        },
        deleteURL: function (idPrime) {
            return "/user/" + idPrime;
        },
        editURL: function (idPrime) {
            return "/user/" + idPrime;
        },
        addURL: function (idPrime) {
            return "/user";
        },
        customRenderView: {
            imgUrl: function (rowNum, idPrime, dataValue) {
                return "<img class='text-center' src='" + dataValue + "' alt='your image' width='50px'/>";
            }
        },
        customRenderEdit: {
            imgUrl: function (rowNum, idPrime, dataValue) {
                return "<input type=\"text\" name=\"imgUrl\" id=\"avatarimg\" placeholder=\"image url\">";
            }
        },
        customSave: {
            imgUrl: function (rowNum, idPrime) {
                var img = document.getElementById("avatarimg");
                return img.value;
            }
        },
        format: "json"
    });
}
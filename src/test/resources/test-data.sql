SET REFERENTIAL_INTEGRITY FALSE;
TRUNCATE TABLE ORDERS;
TRUNCATE TABLE USERS;
SET REFERENTIAL_INTEGRITY TRUE;

INSERT INTO users (id, first_name, last_name, email, phone_number, address, img_url) VALUES
  (1, 'Alex', 'Sanchez', 'rick@gmail.com', '+380666027034', 'Earth C-132', 'https://vignette.wikia.nocookie.net/rickandmorty/images/d/dd/Rick.png'),
  (2, 'Olga', 'Smith', 'morty@gmail.com', '+380123456789', 'New York', 'https://pre00.deviantart.net/a12c/th/pre/i/2016/010/0/a/morty_face_by_kushmastafresh-d9nji2b.png');

INSERT INTO orders (id, product_category, product_name, amount, sum, user_id) VALUES
  (1, 'Toys', 'Infinity spinner', 3, 15, 1),
  (2, 'Watches', 'Hublot', 2, 28500, 1),
  (3, 'Car', 'BMW X6', 1, 42000, 2);
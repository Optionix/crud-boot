package com.shop.userorderapp.api;

import com.shop.userorderapp.ApiTest;
import com.shop.userorderapp.dao.OrderRepository;
import com.shop.userorderapp.dto.OrderCreateDto;
import com.shop.userorderapp.model.Order;
import com.shop.userorderapp.model.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OrderIntegrationTest extends ApiTest {

    @Autowired
    private OrderRepository orderRepository;

    @Test
    public void getOrder() throws Exception {
        Long orderId = 1L;

        mvc.perform(MockMvcRequestBuilders.get("/order/" + orderId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(orderId.intValue())))
                .andExpect(jsonPath("$.productCategory", is("Toys")))
                .andExpect(jsonPath("$.productName", is("Infinity spinner")))
                .andExpect(jsonPath("$.amount", is(3)))
                .andExpect(jsonPath("$.sum", is(15)));
    }

    @Test
    public void orderNotFound() throws Exception {
        long notExistingOrderId = 42L;

        mvc.perform(MockMvcRequestBuilders.get("/order/" + notExistingOrderId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getAllOrders() throws Exception {
        int countOrders = (int) orderRepository.count();

        mvc.perform(
                MockMvcRequestBuilders.get("/order"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.order", hasSize(countOrders)));
    }

    @Test
    @Transactional
    public void createOrder() throws Exception {
        long orderOwnerId = 1L;

        int newOrderId = (int) orderRepository.count() + 1;

        OrderCreateDto newOrder = generateTestOrder();
        newOrder.setUser("/user/" + orderOwnerId);
        String newOrderJson = asJsonString(newOrder);

        mvc.perform(MockMvcRequestBuilders.post("/order")
                .contentType(contentType)
                .content(newOrderJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(newOrderId)))
                .andExpect(jsonPath("$.productCategory", is(newOrder.getProductCategory())))
                .andExpect(jsonPath("$.productName", is(newOrder.getProductName())))
                .andExpect(jsonPath("$.amount", is(newOrder.getAmount())))
                .andExpect(jsonPath("$.sum", is((int) newOrder.getSum())));

        Order createdOrder = orderRepository.findOne((long) newOrderId);
        User orderOwner = createdOrder.getUser();

        assertEquals(1, orderOwner.getId().intValue());
    }

    @Test
    public void updateOrder() throws Exception {
        Long orderIdToUpdate = 1L;

        OrderCreateDto orderForUpdate = generateTestOrder();
        String orderForUpdateJson = asJsonString(orderForUpdate);

        mvc.perform(MockMvcRequestBuilders.put("/order/" + orderIdToUpdate)
                .contentType(contentType)
                .content(orderForUpdateJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(orderIdToUpdate.intValue())))
                .andExpect(jsonPath("$.productCategory", is(orderForUpdate.getProductCategory())))
                .andExpect(jsonPath("$.productName", is(orderForUpdate.getProductName())))
                .andExpect(jsonPath("$.amount", is(orderForUpdate.getAmount())))
                .andExpect(jsonPath("$.sum", is((int) orderForUpdate.getSum())));
    }

    @Test
    @Transactional
    public void deleteOrder() throws Exception {
        long orderId = 1L;

        mvc.perform(MockMvcRequestBuilders.delete("/order/" + orderId))
                .andExpect(status().isNoContent());

        Order deletedOrder = orderRepository.findOne(orderId);

        assertNull(deletedOrder);
    }

    private OrderCreateDto generateTestOrder() {
        OrderCreateDto order = new OrderCreateDto();
        order.setProductCategory("Food");
        order.setProductName("Szechuan sauce");
        order.setAmount(18);
        order.setSum(1800);

        return order;
    }

}

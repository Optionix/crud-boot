package com.shop.userorderapp.api;

import com.shop.userorderapp.ApiTest;
import com.shop.userorderapp.dao.OrderRepository;
import com.shop.userorderapp.dao.UserRepository;
import com.shop.userorderapp.model.Order;
import com.shop.userorderapp.model.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserIntegrationTest extends ApiTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Test
    public void getUser() throws Exception {
        Long userId = 1L;

        mvc.perform(MockMvcRequestBuilders.get("/user/" + userId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(userId.intValue())))
                .andExpect(jsonPath("$.firstName", is("Alex")))
                .andExpect(jsonPath("$.lastName", is("Sanchez")))
                .andExpect(jsonPath("$.email", is("rick@gmail.com")))
                .andExpect(jsonPath("$.phoneNumber", is("+380666027034")))
                .andExpect(jsonPath("$.address", is("Earth C-132")));
    }

    @Test
    public void userNotFound() throws Exception {
        long notExistingUserId = 3L;

        mvc.perform(MockMvcRequestBuilders.get("/user/" + notExistingUserId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getAllUsers() throws Exception {
        int countUsers = (int) userRepository.count();

        mvc.perform(
                MockMvcRequestBuilders.get("/user"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.user", hasSize(countUsers)));
    }

    @Test
    public void createUser() throws Exception {
        User newUser = generateTestUser();
        String newUserJson = asJsonString(newUser);

        mvc.perform(MockMvcRequestBuilders.post("/user")
                .contentType(contentType)
                .content(newUserJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName", is(newUser.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(newUser.getLastName())))
                .andExpect(jsonPath("$.email", is(newUser.getEmail())))
                .andExpect(jsonPath("$.phoneNumber", is(newUser.getPhoneNumber())))
                .andExpect(jsonPath("$.address", is(newUser.getAddress())));
    }

    @Test
    public void updateUser() throws Exception {
        Long userIdToUpdate = 1L;

        User userForUpdate = generateTestUser();
        String userForUpdateJson = asJsonString(userForUpdate);

        mvc.perform(MockMvcRequestBuilders.put("/user/" + userIdToUpdate)
                .contentType(contentType)
                .content(userForUpdateJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(userIdToUpdate.intValue())))
                .andExpect(jsonPath("$.firstName", is(userForUpdate.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(userForUpdate.getLastName())))
                .andExpect(jsonPath("$.email", is(userForUpdate.getEmail())))
                .andExpect(jsonPath("$.phoneNumber", is(userForUpdate.getPhoneNumber())))
                .andExpect(jsonPath("$.address", is(userForUpdate.getAddress())));
    }

    @Test
    @Transactional
    public void deleteUserShouldAlsoDeleteAllUsersOrders() throws Exception {
        long userId = 1L;

        User user = userRepository.findOne(userId);
        List<Order> userOrderList = user.getOrders();

        mvc.perform(MockMvcRequestBuilders.delete("/user/" + userId))
                .andExpect(status().isNoContent());

        User deletedUser = userRepository.findOne(userId);

        userOrderList.forEach(order -> assertNull(orderRepository.findOne(order.getId())));
        assertNull(deletedUser);
    }

    private User generateTestUser() {
        User user = new User();
        user.setFirstName("Alex");
        user.setLastName("Ko");
        user.setAddress("some address");
        user.setEmail("test@gmail.com");
        user.setPhoneNumber("+380666027034");
        user.setImgUrl("https://vignette.wikia.nocookie.net/rickandmorty/images/d/dd/Rick.png");

        return user;
    }

}

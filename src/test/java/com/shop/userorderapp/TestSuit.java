package com.shop.userorderapp;

import com.shop.userorderapp.api.OrderIntegrationTest;
import com.shop.userorderapp.api.UserIntegrationTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        UserIntegrationTest.class,
        OrderIntegrationTest.class
})
public class TestSuit {
}

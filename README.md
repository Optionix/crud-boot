# Interdimensional shop

Light-weight web application with CRUD REST service for User and Order entities.
Spring Boot data rest was chosen for this purpose. Also for all CRUD endpoints **integrational mvc tests** were made.
There are **no service layer** because spring-boot-starter-data-rest adds CRUD REST out-of-the-box. In our test case service layer would be redundant.

### Bonus
Simple CRUD table for users: http://localhost:8080

H2 client: http://localhost:8080/h2